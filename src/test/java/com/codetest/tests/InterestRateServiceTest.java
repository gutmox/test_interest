package com.codetest.tests;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.codetest.CurrentRates;
import com.codetest.dto.BaseRate;
import com.codetest.dto.SpecialRate;
import com.codetest.enums.ClientType;
import com.codetest.impl.InterestRateServiceImpl;
import com.codetest.pojos.BaseInterestRate;
import com.codetest.repositories.BaseInterestRateRepository;

@RunWith(MockitoJUnitRunner.class)
public class InterestRateServiceTest {
	
	@InjectMocks
	private InterestRateServiceImpl interestRateService;
	
	@Mock
	private BaseInterestRateRepository baseInterestRateRepository;
	
	@Mock
	private CurrentRates currentRates;
	
	@Mock
	BaseInterestRate baseInterestRate;
	
	@Test
	public void shouldReturnSpecialRate() {
		// Fixture
		ClientType clientType = ClientType.SPECIAL;
		when(currentRates.getCurrentRates()).thenReturn(baseInterestRate);
		when(baseInterestRate.getPrivilegeInterestRate()).thenReturn(1F);
		when(baseInterestRate.getSpecialInterestRate()).thenReturn(2F);
		when(baseInterestRate.getStandarInterestRate()).thenReturn(1F);
		//Experimentation
		Float calculatedRate = interestRateService.calculateRateByClienType(clientType);
		//Expectations
		assertEquals(2F, calculatedRate.floatValue(), 0);
	}
	
	@Test
	public void shouldReturnPrivilegeRate() {
		// Fixture
		ClientType clientType = ClientType.PRIVILIGED;
		when(currentRates.getCurrentRates()).thenReturn(baseInterestRate);
		when(baseInterestRate.getPrivilegeInterestRate()).thenReturn(2F);
		when(baseInterestRate.getSpecialInterestRate()).thenReturn(1F);
		when(baseInterestRate.getStandarInterestRate()).thenReturn(1F);
		//Experimentation
		Float calculatedRate = interestRateService.calculateRateByClienType(clientType);
		//Expectations
		assertEquals(2F, calculatedRate.floatValue(), 0);
	}
	
	@Test
	public void shouldReturnStandarRate() {
		// Fixture
		ClientType clientType = ClientType.STANDARD;
		when(currentRates.getCurrentRates()).thenReturn(baseInterestRate);
		when(baseInterestRate.getPrivilegeInterestRate()).thenReturn(1F);
		when(baseInterestRate.getSpecialInterestRate()).thenReturn(1F);
		when(baseInterestRate.getStandarInterestRate()).thenReturn(2F);

		//Experimentation
		Float calculatedRate = interestRateService.calculateRateByClienType(clientType);
		//Expectations
		assertEquals(2F, calculatedRate.floatValue(), 0);
	}

	@Test
	public void shouldSetNewBaseRate() {
		
		// Fixture
		BaseRate baseRate = new BaseRate(1F);
		//Experimentation
		interestRateService.setNewBaseRate(baseRate);		
		//Expectations
		verify(baseInterestRateRepository).save(new BaseInterestRate(baseRate.getBaseInterestRate()));
	}

	@Test
	public void shouldSetNewSpecialRate() {
		// Fixture
		SpecialRate specialRate = new SpecialRate(1F);
		when(currentRates.getCurrentRates()).thenReturn(baseInterestRate);
		//Experimentation
		interestRateService.setNewSpecialRate(specialRate);		
		//Expectations
		verify(baseInterestRateRepository).save(baseInterestRate);
	}

}
