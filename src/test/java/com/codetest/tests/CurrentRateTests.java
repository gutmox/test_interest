package com.codetest.tests;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import com.codetest.impl.CurrentRatesImpl;
import com.codetest.pojos.BaseInterestRate;
import com.codetest.repositories.BaseInterestRateRepository;

@RunWith(MockitoJUnitRunner.class)
public class CurrentRateTests {
	
	@InjectMocks
	private CurrentRatesImpl currentRates;
	
	@Mock
	private BaseInterestRateRepository baseInterestRateRepository;

	@Mock
	private Page<BaseInterestRate> lastElement;
	
	@Mock
	private List<BaseInterestRate> lastElementList;
	
	@Mock
	private BaseInterestRate lastRate;
	
	@Test
	public void shouldReturnCurrentRates() {
		// Fixture
		Order order = new Order(Direction.DESC, "creationDate");
		Pageable pageable = new PageRequest(0, 1, new Sort(order)); 
		when(lastElement.getContent()).thenReturn(lastElementList);
		when(lastElementList.get(0)).thenReturn(lastRate);
		
		//Experimentation
		BaseInterestRate currentRatesReturned = currentRates.getCurrentRates();		
		//Expectations
		verify(baseInterestRateRepository).findAll(pageable);
		assertEquals(lastRate, currentRatesReturned);
	}

}
