package com.codetest.tests;

import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.codetest.InterestRateService;
import com.codetest.dto.BaseRate;
import com.codetest.rest.BaseInterestRateResource;

@RunWith(MockitoJUnitRunner.class)
public class BaseInterestRateResourceTest {
	
	@InjectMocks
	private BaseInterestRateResource baseInterestRateResource;

	@Mock
	private InterestRateService interestRateService;

	@Mock
	private BaseRate baseRate;
	
	@Test
	public void shouldSetNewRate() {
		// Fixture
		
		//Experimentation
		baseInterestRateResource.setNewBaseRate(baseRate);
		//Expectations
		verify(interestRateService).setNewBaseRate(baseRate);
	}

}
