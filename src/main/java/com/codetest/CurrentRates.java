package com.codetest;

import com.codetest.pojos.BaseInterestRate;

public interface CurrentRates {

	BaseInterestRate getCurrentRates();

}
